/*
== Definition: postfix::transport_regexp_snippet

Adds a transport_regexp snippets to /etc/postfix/transport_regexp.
See the postfix::transport_regexp class for details.

Parameters:
- *source* or *content*: source or content of the transport_regexp snippet

Requires:
- Class["postfix"]

Example usage:

  node "toto.example.com" {
    class { 'postfix': }
    postfix::transport_regexp {
      'wrong_date': content => 'FIXME';
      'bla':        source => 'puppet:///files/etc/postfix/transport_regexp.d/bla';
    }
  }

*/

define postfix::transport_regexp_snippet (
  $source = '',
  $content = undef
) {

  if $source == '' and $content == undef {
    fail("One of \$source or \$content must be specified for postfix::transport_regexp_snippet ${name}")
  }

  if $source != '' and $content != undef {
    fail("Only one of \$source or \$content must specified for postfix::transport_regexp_snippet ${name}")
  }

  if $value == false {
    fail("The value parameter must be set when using the postfix::transport_regexp_snippet define.")
  }

  include postfix::transport_regexp

  $snippetfile = "${postfix::transport_regexp::postfix_transport_regexp_snippets_dir}/${name}"
  
  file { "$snippetfile":
    mode    => 600,
    owner   => root,
    group   => 0,
    notify => Exec["concat_${postfix::transport_regexp::postfix_merged_transport_regexp}"],
  }

  if $source {
    File["$snippetfile"] {
      source => $source,
    }
  }
  else {
    File["$snippetfile"] {
      content => $content,
    }
  }

}
